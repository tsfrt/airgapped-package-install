## Internet connected

### install imgpkg
- included with the tanzu cli or download from github

https://github.com/vmware-tanzu/carvel-imgpkg/releases


### check the package repo image reference from a current TKG install, this is for TKG 1.4
`kubectl get pkgr -n tanzu-package-repo-global tanzu-standard -o yaml`

```
#this is the image reference for 1.4 so you don't need to look it up
projects.registry.vmware.com/tkg/packages/standard/repo@sha256:ff1f86ec509bc9be7007a441d52dab6df68ab4741f25d312404b8a3e57030aaf

```

### Create a tar file containing Packagerepository
- this will downlod all dependent images and config for xfer
- this will create a tar file that you need to xfer over
- take note of the hash at the end, you will use this on the other side `@sha256:ff1f86ec509bc9be7007a441d52dab6df68ab4741f25d312404b8a3e57030aaf`
```

imgpkg copy -b projects.registry.vmware.com/tkg/packages/standard/repo@sha256:ff1f86ec509bc9be7007a441d52dab6df68ab4741f25d312404b8a3e57030aaf --to-tar tkg-1.4.1-tanzu-standard.tar

```

### Moving to airgapped
- move the imgpkg cli binary and tar file over to your airgapped environment

## Airgapped

### Upload to local registry
- install imgpkg CLI if not already there
- replace `harbor.proto.tsfrt.net` with your registry and `apps` with your project
- copy the bundle into your local registry
```

imgpkg copy --tar tkg-1.4.1-tanzu-standard.tar --to-repo harbor.proto.tsfrt.net/apps/tanzu-standard

```

### Delpoy packagerepo to cluster
- create the namespace if it doesn't exist
- create PackageRepository yaml
- apply PackageRepository yaml

```

apiVersion: packaging.carvel.dev/v1alpha1
kind: PackageRepository
metadata:
  name: tanzu-standard
  namespace: test-namespace
spec:
  fetch:
    imgpkgBundle:
      image: harbor.proto.tsfrt.net/apps/tanzu-standard@sha256:ff1f86ec509bc9be7007a441d52dab6df68ab4741f25d312404b8a3e57030aaf;

#pkgr is a file with the content above
kubectl apply -f pkgr.yaml

```

### validate package repository
- should say `Reconcile succeeded` when ready

```

kubectl get pkgr -A

```


### Follow standard install instructions
https://docs.vmware.com/en/VMware-Tanzu-Kubernetes-Grid/1.4/vmware-tanzu-kubernetes-grid-14/GUID-packages-harbor-registry.html


